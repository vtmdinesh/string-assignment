
const string1Function = (...args) => {

    let stringArray = [...args]
    let numbersArray = []
  
    const checkIntPart = (item)=> item.length <=2;

    // Function to Convert string into a float value

    const findFloatValue = (value) => {   
        value  = value.replaceAll(",",'')
        const num = parseFloat(value)
        
        if (value.length === (num.toString().length)) {
            numbersArray.push(num)
        }
        else {
            numbersArray.push(0)
        }
    }
    
    stringArray.forEach(item => {

        // Removing $ symbol
        let value = item.replace("$","")
      
        //Checking Whether the value have "." in it or not 
        if(item.includes(".")){
            let valueArray = value.split(".")
            
            // If more than one "." presents in value it will be considered as invalid input and 0 will be pushed
            if (valueArray.length > 2) {
                numbersArray.push(0)
            }
            // If the value does not have "," in it, that will be send directly into find float value function 
            else if (!item.includes(",")) {
                findFloatValue(value)
            }

            // If the value does have ","  

            else{

                let decimalArray = valueArray[1].split(",") 
                
                // Checking in decimal part any "," presents, if true the number will be considered as invalid and 0 will be pushed
                if (decimalArray.length !==1){
                    numbersArray.push(0)
                }
            
                else {
                    let intPart = valueArray[0].split(",")
                    
                    // If no of comma is 1 , the number will be send directly to convert to float function
                    if (intPart.length == 1 )
                    {
                        findFloatValue(value);              
                    }
                    else {
                  
                        // Checking Whether the integer part properly seperated by Comma or not
                        if ((intPart[intPart.length-1].length===3) && intPart.slice(0,intPart.length-1).every(checkIntPart)) {
                            findFloatValue(value);
                        }
                        else {
                            numbersArray.push(0)
                        } 
                        
                    } 
                } 
            }
        }
        else{
            findFloatValue(value)
    }
    }) 

    return numbersArray
}

module.exports = string1Function