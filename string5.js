const string5Function = (stringArray) => {
     
   let joinedString = ""

   for (let i = 0;i<stringArray.length;i++){
    joinedString = joinedString + " " + stringArray[i]

   } 
     
    // Removing whitespaces at the starting of string
    joinedString = joinedString.trim()

    if (joinedString !== ""){
        return joinedString
    }
    else {
        // "" Provided to indicate empty string 
        return `""`
    }
    
    
}

module.exports = string5Function