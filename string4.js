const string4Function = (obj) => {
     
    let fullName = "";
    let partOfName = "";
    
    for (let name in obj) {
        partOfName = obj[name]
        
        fullName = fullName + " " + partOfName[0].toUpperCase() + partOfName.slice(1).toLowerCase()

    }
    
    fullName = fullName.trim()
    return fullName
}

module.exports = string4Function