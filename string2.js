const string2Function = (ip) => {
    const arr = ip.split(".")
    let ipArray = []
    let num;

    arr.forEach(item => {
        num = parseInt(item)
        
        // Checking is any characters present in IP
        if (num.toString().length === item.length && !Number.isNaN(num)){
                ipArray.push(num) 
            }
        else {
            ipArray = []
        } 
    
    }) 
    // Checking number of parts in given IP   
    if (ipArray.length === 4){
        return ipArray
    }
    else {
        return []
    }
}

module.exports = string2Function